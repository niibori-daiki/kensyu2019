<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
  <?php
      $DB_DSN = "mysql:host=localhost; dbname=niiboridb; charset=utf8";
      $DB_USER = "test";
      $DB_PW = "pas";
      $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
      $query_str = "SELECT m.id AS mid, m.position AS position, m.origin AS origin, m.seibetu AS seibetu, m.department AS department, age, m.name AS name, om.ori_name,
                           om.id, po.id, de.id, de.department, po.position
                    FROM member AS m LEFT JOIN pos_mst AS po ON po.id = m.position
                                LEFT JOIN ori_mst AS om ON om.id = m.origin
                                LEFT JOIN dep_mst AS de ON de.id = m.department";
          //初回表示は末尾消去をせず、検索項目に入力がある場合
          if(!empty($_GET['kenname']) || !empty($_GET['kenposition']) || !empty($_GET['kenorigin']) || !empty($_GET['kenseibetu']) || !empty($_GET['kenage']) || !empty($_GET['kendepartment'])){

          if(isset($_GET['kenname'])){
            $kensaku = $_GET['kenname'];
            $query_str .= " WHERE name LIKE '%" . $kensaku . "%' AND";
          }
          if(isset($_GET['kenorigin'])){
            $kenori = $_GET['kenorigin'];
            $query_str .= " m.origin LIKE '%" . $kenori . "%' AND";
          }
          if(isset($_GET['kenseibetu'])){
            $kensei = $_GET['kenseibetu'];
            $query_str .= " m.seibetu LIKE '%" . $kensei . "%' AND";
          }
          if(isset($_GET['kenage'])){
            $kage = $_GET['kenage'];
            $query_str .= " m.age LIKE '%" . $kage . "%' AND";
          }
          if(isset($_GET['kendepartment'])){
            $kendep = $_GET['kendepartment'];
            $query_str .= " m.department LIKE '%" . $kendep . "%' AND";
          }
          if(isset($_GET['kenposition'])){
            $kenpos = $_GET['kenposition'];
            $query_str .= " m.position LIKE '%" . $kenpos . "%' AND";
          }
            $query_str = substr("$query_str",0,-4);
          }
          $query_str .= " ORDER BY m.id    ";

      $sql = $pdo->prepare($query_str);
      $sql->execute();
      $result = $sql->fetchALL();
      // SQL文表示
      // echo $query_str;
  ?>
<html>
  <head>
      <meta charset="utf-utf-8">
      <meta name="viewport" content="width=device-width, initail-scale=1">
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <title>社員表</title>
    </head>
    <script type="text/javascript">
    <!--
    function condReset(){
      document.forms["search"].elements["kenname"].value = "";
      document.forms["search"].elements["kenorigin"].value = "";
      document.forms["search"].elements["kenseibetu"].value = "";
      document.forms["search"].elements["kenage"].value = "";
      document.forms["search"].elements["kendepartment"].value = "";
      document.forms["search"].elements["kenposition"].value = "";
    }
    -->
    </script>
    <body>
      <h1> 社員検索画面</h1>
      <div align="right">| <a href = index.php>トップ画面</a> | <a href = entry01.php>新規登録画面</a> | </div>
      <form method="GET" action="index.php" name="search">
        <!-- 検索ボックス -->
        <table border="1" align="center" cellspacing="0" width='500'>
            <tr><th bgcolor="4169e1" style="color:#ffffff">名前</th><td width='300'><input type="text" name="kenname" value="<?php if(isset($_GET['kenname'])){ echo $_GET['kenname'];}else{echo "";} ?>"></td></tr>
              <tr><th bgcolor="4169e1"style="color:#ffffff">出身地</th><td><select name="kenorigin">
                                     <option value="" <?php if(isset($_GET['kenorigin'])){if($_GET['kenorigin'] == ""){ echo "selected";}else{echo "";}}?>>出身地</option>
                                     <option value="1" <?php if(isset($_GET['kenorigin'])){if($_GET['kenorigin'] == "1"){ echo "selected";}else{echo "";}}?>>北海道</option>
                                     <option value="2" <?php if(isset($_GET['kenorigin'])){if($_GET['kenorigin'] == "2"){ echo "selected";}else{echo "";}}?>>東京都</option>
                                     <option value="3" <?php if(isset($_GET['kenorigin'])){if($_GET['kenorigin'] == "3"){ echo "selected";}else{echo "";}}?>>沖縄</option></td></tr>
              <tr><th width='150' bgcolor="4169e1"style="color:#ffffff">性別</th><td><input type="radio" name="kenseibetu" value="1" <?php if(isset($_GET['kenseibetu'])){if($_GET['kenseibetu'] == "1"){echo "checked";}}else{ echo "";} ?>>男
                                                                <input type="radio" name="kenseibetu" value="2" <?php if(isset($_GET['kenseibetu'])){if($_GET['kenseibetu'] == "2"){echo "checked";}}else{ echo "";} ?>>女</td></tr>
              <tr><th width='150' bgcolor="4169e1"style="color:#ffffff">年齢</th><td><input type="text" name="kenage" value=<?php if(isset($_GET['kenage'])){ echo $_GET['kenage'];}else{ echo "";} ?>></td></tr>
              <tr><th width='150' bgcolor="4169e1"style="color:#ffffff">所属部署</th><td><select name="kendepartment">
                                 <option value=""<?php if(isset($_GET['kendepartment'])){if($_GET['kendepartment'] == ""){ echo "selected";}}?>>所属部署</option>
                                 <option value="1"<?php if(isset($_GET['kendepartment'])){if($_GET['kendepartment'] == "1"){ echo "selected";}}?>>第一事業部</option>
                                 <option value="2"<?php if(isset($_GET['kendepartment'])){if($_GET['kendepartment'] == "2"){ echo "selected";}}?>>第二事業部</option>
                                 <option value="3"<?php if(isset($_GET['kendepartment'])){if($_GET['kendepartment'] == "3"){ echo "selected";}}?>>営業</option>
                                 <option value="4"<?php if(isset($_GET['kendepartment'])){if($_GET['kendepartment'] == "4"){ echo "selected";}}?>>総務</option>
                                 <option value="5"<?php if(isset($_GET['kendepartment'])){if($_GET['kendepartment'] == "5"){ echo "selected";}}?>>人事</option></td></tr>
              <tr><th width='150' bgcolor="4169e1"style="color:#ffffff">役職</th><td><select name="kenposition">
                                  <option value=""<?php if(isset($_GET['kenposition'])){if($_GET['kenposition'] == ""){ echo "selected";}}?>>役職</option>
                                  <option value="1"<?php if(isset($_GET['kenposition'])){if($_GET['kenposition'] == "1"){ echo "selected";}}?>>メンバー</option>
                                  <option value="2"<?php if(isset($_GET['kenposition'])){if($_GET['kenposition'] == "2"){ echo "selected";}}?>>リーダー</option>
                                  <option value="3"<?php if(isset($_GET['kenposition'])){if($_GET['kenposition'] == "3"){ echo "selected";}}?>>チームリーダー</option>
                                  <option value="4"<?php if(isset($_GET['kenposition'])){if($_GET['kenposition'] == "4"){ echo "selected";}}?>>部長</option>
                                  <option value="5"<?php if(isset($_GET['kenposition'])){if($_GET['kenposition'] == "5"){ echo "selected";}}?>>事業部長</option></td></tr>
        </table>
        <div align="center"><input type=submit value="検索">  <input type=button onclick="return condReset()" value="リセット"></div>
      </form>
      <hr/>
        <?php
       $count = count($result,);
        echo "<div width='300px' style='width:700px; margin: 0 auto;'>検索人数";

        if(isset($count)){
          echo $count . "人";
        }else{
          echo "0 人";
        }
        echo "</div><br>";
        echo "<table border='1' align='center' cellspacing='0' width='700'>";

        // echo "<pre>";
        // var_dump($result);
        // echo "</pre>";
        echo "<tr align='center'><th width='150' bgcolor='4169e1'style='color:#ffffff'>名前</th><th width='150' bgcolor='4169e1'style='color:#ffffff'>部署</th><th width='150' bgcolor='4169e1'style='color:#ffffff'>役職</th><th width='100' bgcolor='4169e1'style='color:#ffffff'>性別</th><th width='100' bgcolor='4169e1'style='color:#ffffff'>出身地</th><th width='50' bgcolor='4169e1'style='color:#ffffff'>年齢</th><tr>";
        foreach($result as $each){
         echo "<tr><td>";
         echo "<a href = 'detail01.php?id=" . $each['mid'] . "'>";
         echo $each['name'] . "</a></td><td>" . $each['department'] . "</td><td>" . $each['position'] . "</td><td>";
              if($each['seibetu'] == '1'){
                echo '男';
              }else{
                echo '女';
              }
          echo "</td><td>" . $each['ori_name'] . "</td><td>" . $each['age'];
         echo "</td><tr>";
         // echo "<hr/>";

        }

        ?>
   </table>
   <!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
