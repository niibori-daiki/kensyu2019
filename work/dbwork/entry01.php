<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">

<html>
  <head>
    <meta name="viewport" content="width=device-width, initail-scale=1">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <meta charset="utf-utf-8">
      <meta name="viewport" content="width=device-width, initail-scale=1">
      <title>社員表</title>
      <script type="text/javascript">
        <!--
        function conf(){
          if(window.confirm('新規登録を行います。よろしいですか？')){
             document.entry.submit();
          }
        }
        -->
      </script>
    </head>
    <body>
      <h1>社員登録画面</h1>
      <div align="right">| <a href = index.php>トップ画面</a> |</div>
      <form method="POST" action="entry02.php" name="entry">
        <!-- 検索ボックス -->
        <table align="center" border="1" cellspacing="0">
            <tr><th width='250' bgcolor="4169e1"style="color:#ffffff">名前</th><td width='500'><input type="text" name="toname"></td></tr>
              <tr><th width='250' bgcolor="4169e1"style="color:#ffffff">出身地</th><td><select name="toorigin">
                                     <option value="">出身地</option>
                                     <option value="1">北海道</option>
                                     <option value="2">東京都</option>
                                     <option value="3">沖縄</option></td></tr>
              <tr><th width='250' bgcolor="4169e1"style="color:#ffffff">性別</th><td><input type="radio" name="toseibetu" value="1" checked>男
                                   <input type="radio" name="toseibetu" value="2">女</td></tr>
              <tr><th width='250' bgcolor="4169e1"style="color:#ffffff">年齢</th><td><input type="text" name="toage"></td></tr>
              <tr><th width='250' bgcolor="4169e1"style="color:#ffffff">所属部署</th><td><select name="todepartment">
                                   <option value="">所属部署</option>
                                   <option value="1">第一事業部</option>
                                   <option value="2">第二事業部</option>
                                   <option value="3">営業</option>
                                   <option value="4">総務</option>
                                   <option value="5">人事</option></td></tr>
              <tr><th width='250' bgcolor="4169e1"style="color:#ffffff">役職</th><td><select name="toposition">
                                  <option value="">役職</option>
                                  <option value="1">メンバー</option>
                                  <option value="2">リーダー</option>
                                  <option value="3">チームリーダー</option>
                                  <option value="4">部長</option>
                                  <option value="5">事業部長</option></td></tr>
            </tr>
        </table>
          <div align="center"><input type=button value="登録" onClick="conf();">  <input type=reset value="リセット"></div>
      </form>
      <hr/>

  </body>
</html>
