<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>第二回課題、フォーム部品練習</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
<?php
// echo "<pre>";
// var_dump($_POST);
// echo "</pre>";

//一段目商品価格
//  echo $_POST['1shouhin'];
    $price1 = $_POST['1kakaku'];
    $kosup1 = $_POST['1kosu'];
    $zei1 = $_POST['1zei'];
    $price1 = $price1 * $kosup1;
    $pricezei1 = $price1 * $zei1;
    $Aprice1 = $price1 + $pricezei1;
//  echo $price ."円（税込み）";

//二段目商品価格
    $price2 = $_POST['2kakaku'];
    $kosup2 = $_POST['2kosu'];
    $zei2 = $_POST['2zei'];
    $price2 = $price2 * $kosup2;
    $pricezei2 = $price2 * $zei2;
    $Aprice2 = $price2 + $pricezei2;

//三段目商品価格
    $price3 = $_POST['3kakaku'];
    $kosup3 = $_POST['3kosu'];
    $zei3 = $_POST['3zei'];
    $Aprice3 = $price3 * $kosup3;
    $pricezei3 = $price3 * $zei3;
    $price3 = $price3 + $pricezei3;

//四段目商品価格
    $price4 = $_POST['4kakaku'];
    $kosup4 = $_POST['4kosu'];
    $zei4 = $_POST['4zei'];
    $price4 = $price4 * $kosup4;
    $pricezei4 = $price4 * $zei4;
    $Aprice4 = $price4 + $pricezei4;
//五段目商品価格
    $price5 = $_POST['5kakaku'];
    $kosup5 = $_POST['5kosu'];
    $zei5 = $_POST['5zei'];
    $price5 = $price5 * $kosup5;
    $pricezei5 = $price5 * $zei5;
    $Aprice5 = $price5 + $pricezei5;

//合計
    $pricesum = $Aprice1 + $Aprice2 + $Aprice3 + $Aprice4 + $Aprice5;
?>
<table border="1">
  <tr>
      <th>商品名</th><th>価格（単位：円、税抜き）</th><th width="50">個数</th><th>税率</th><th>小計</th>
  </tr>
  <tr>
          <td><?php echo $_POST['1shouhin'];?></td>
          <td><?php echo $_POST['1kakaku'];?></td>
          <td><?php echo $_POST['1kosu'];?></td>
          <td><?php echo $_POST['1zei'];?></td>
          <td><?php echo $Aprice1; ?></td>
  </tr>
  <tr>
          <td><?php echo $_POST['2shouhin'];?></td>
          <td><?php echo $_POST['2kakaku'];?></td>
          <td><?php echo $_POST['2kosu'];?></td>
          <td><?php echo $_POST['2zei'];?></td>
          <td><?php echo $Aprice2; ?></td>
  </tr>
  <tr>
          <td><?php echo $_POST['3shouhin'];?></td>
          <td><?php echo $_POST['3kakaku'];?></td>
          <td><?php echo $_POST['3kosu'];?></td>
          <td><?php echo $_POST['3zei'];?></td>
          <td><?php echo $Aprice3; ?></td>
  </tr>
  <tr>
          <td><?php echo $_POST['4shouhin'];?></td>
          <td><?php echo $_POST['4kakaku'];?></td>
          <td><?php echo $_POST['4kosu'];?></td>
          <td><?php echo $_POST['4zei'];?></td>
          <td><?php echo $Aprice4; ?></td>
  </tr>
  <tr>
          <td><?php echo $_POST['5shouhin'];?></td>
          <td><?php echo $_POST['5kakaku'];?></td>
          <td><?php echo $_POST['5kosu'];?></td>
          <td><?php echo $_POST['5zei'];?></td>
          <td><?php echo $Aprice5; ?></td>
  </tr>
  <tr>
    <td colspan="4">合計</td>
    <td><?php echo $pricesum; ?></td>
  </tr>

</body>
</html>
